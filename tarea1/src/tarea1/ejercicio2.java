/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Jesús
 */
public class ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int matriz[][],nFilas,nCol,sumaFilas,sumaCol;
        
        nFilas=Integer.parseInt(JOptionPane.showInputDialog("Digite el numero de filas"));
        nCol=Integer.parseInt(JOptionPane.showInputDialog("Digite el numero de columnas"));
        
        matriz = new int [nFilas][nCol];
        
        System.out.println("Digite la matriz");
        for (int i = 0; i < nFilas; i++) {
            for (int j = 0; j < nCol; j++) {
                System.out.print("Matriz["+i+"]["+j+"]");
                matriz[i][j] = entrada.nextInt();                
            }
            
        }
        
        System.out.println("\nLa Matriz es: ");
        for (int i = 0; i < nFilas; i++) {
            for (int j = 0; j < nCol; j++) {
                
                System.out.print(matriz[i][j]+" ");   
            }
            System.out.println(" ");
        }
        
        //sumamos las filas
        for (int i = 0; i < nFilas; i++) {
            sumaFilas = 0;
            for (int j = 0; j < nCol; j++) {
                sumaFilas += matriz[i][j];
                
            }
            System.out.print("\nLa suma de la fila["+i+"] es: "+sumaFilas);
            int resultado=0;
            if(sumaFilas %2==0){
                System.out.println(".");
            }
            else{
                System.out.println("...");
            }
             
             
        }
        
        System.out.println("");
        
        
        
        
    }
    
}

